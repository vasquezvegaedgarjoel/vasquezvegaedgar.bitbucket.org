<?php 

/* Librería: http://www.fpdf.org
 * Es una librería muy anticuada, del 2011, sin embargo
 * para demostrar de forma básica la integración de otra
 * librería nos es más que suficiente.
 */
require "aplicacion/librerias/pdf/fpdf.php";

$documento_pdf = new FPDF;
$documento_pdf->SetFont('Arial', '', 12);
$documento_pdf->AddPage('L', 'Letter');

$documento_pdf->Cell(0, 10, 'Ejemplares', 0, 0, 'C');

$documento_pdf->Ln();



    $documento_pdf->Cell(180, 7, utf8_decode('Observaciones'), 1);
    $documento_pdf->Cell(50, 7, utf8_decode('ISBN'), 1);


$documento_pdf->Ln();

foreach ($datos['ejemplares'] as $ejemplar) {    
    
        $documento_pdf->Cell(180, 7, utf8_decode($ejemplar['observaciones_ejemplar']), 1);
        $documento_pdf->Cell(50, 7, utf8_decode($ejemplar['isbn']), 1);
    
    $documento_pdf->Ln();
}
$documento_pdf->Output('ejemplares.pdf', 'D');
