<?php 

/* Librería: http://www.fpdf.org
 * Es una librería muy anticuada, del 2011, sin embargo
 * para demostrar de forma básica la integración de otra
 * librería nos es más que suficiente.
 */
require "aplicacion/librerias/pdf/fpdf.php";

$documento_pdf = new FPDF;
$documento_pdf->SetFont('Arial', '', 12);
$documento_pdf->AddPage('L', 'Legal');

$documento_pdf->Cell(0, 10, 'Libros', 0, 0, 'C');

$documento_pdf->Ln();

        $documento_pdf->Cell(180, 7, utf8_decode('Titulo Libro'), 1);
        $documento_pdf->Cell(50, 7, utf8_decode('ISBN '), 1);
        $documento_pdf->Cell(50, 7, utf8_decode('Editorial'), 1);
        $documento_pdf->Cell(50, 7, utf8_decode('Año Publicacion'), 1);

$documento_pdf->Ln();

foreach ($datos['libros'] as $libro) { 
        $documento_pdf->Cell(180, 7, utf8_decode($libro['titulo_libro']), 1);
        $documento_pdf->Cell(50, 7, utf8_decode($libro['isbn_libro']), 1);
        $documento_pdf->Cell(50, 7, utf8_decode($libro['editorial_libro']), 1);
        $documento_pdf->Cell(50, 7, utf8_decode($libro['anio_publicacion_libro']), 1);
    
    $documento_pdf->Ln();
}
$documento_pdf->Output('libros.pdf', 'D');
