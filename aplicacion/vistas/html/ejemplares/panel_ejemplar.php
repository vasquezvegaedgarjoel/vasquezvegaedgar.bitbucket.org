<?php foreach ($datos['ejemplares'] as $ejemplar) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		Autor: <strong><?php echo $ejemplar['observaciones_ejemplar']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>ISBN:</strong> <?php echo $ejemplar['isbn']; ?></li>
			
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
                     <?php if($_SESSION['rol']=='admin_level_1'){ ?>
			<a href="index.php?c=ejemplares_controller&a=editar_ejemplar&v=<?php echo $datos['vista']['tipo_vista'];?>&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-default">Editar</a>
			<a href="index.php?c=ejemplares_controller&a=borrar_ejemplar&v=<?php echo $datos['vista']['tipo_vista'];?>&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-warning">Borrar</a>
                        <?php
                     }else{ ?>
                        <a href="index.php?c=ejemplares_controller&a=ver_lista&v=<?php echo $datos['vista']['tipo_vista'];?>&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-default">Aceptar</a>
                     <?php } ?>
                </div>
	</div>
</div>
<?php } ?>
