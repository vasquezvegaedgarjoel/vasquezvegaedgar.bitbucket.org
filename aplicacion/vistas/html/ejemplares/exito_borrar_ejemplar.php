<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title">
			<strong>Exito</strong>
		</h3>
	</div>
	<div class="panel-body">
		<ul>
			<li>El ejemplar ha sido eliminado exitosamente....	</li>
		</ul>
	</div>
	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="index.php?c=ejemplares_controller&a=ver_lista&v=<?php echo @$datos['vista']['tipo_vista']; ?>"
				class="btn btn-primary">Aceptar</a>
		</div>
	</div>
</div>
