<?php if($_SESSION['rol']=='admin_level_1'){?>
<div class="row">
	<div class="col-md-2">
		<a href="index.php?c=ejemplares_controller&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="index.php?c=ejemplares_controller&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>
<?php } ?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Autores</caption>
			<thead>
				<tr>
					<th>Ejemplar</th>
					
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['ejemplares'] as $ejemplar) { ?>
    <tr>
					<td><?php echo $ejemplar['observaciones_ejemplar']; ?></td>
                                       
					<td><a
						href="index.php?c=ejemplares_controller&a=ver_ejemplar&v=tabla&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-default btn-xs">Información</a>
                                            <?php if($_SESSION['rol']=='admin_level_1'){ ?>
                                            <a
						href="index.php?c=ejemplares_controller&a=editar_ejemplar&v=tabla&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-default btn-xs">Editar</a> <a
						href="index.php?c=ejemplares_controller&a=borrar_ejemplar&v=tabla&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-default btn-xs">Borrar</a>
                                            <?php } ?>
                                        </td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>

