﻿
<div class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Biblioteca</span>
				<span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Biblioteca</a>
		</div>
               <?php if(!empty($_SESSION['usuario'])){ ?>
                    
		<div class="navbar-collapse collapse">
                    
                    
                 <!--comienza la lista de elementos del menu*/-->
			<ul class="nav navbar-nav navbar-left">
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Autores<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Lista</li>
						<li><a href="index.php?c=autores&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="index.php?c=autores&a=ver_lista&v=panel">Paneles</a></li>
						 <?php if($_SESSION['rol']=='admin_level_1'){?>
                                                <li class="dropdown-header">Opciones</li>
						<li><a href="index.php?c=autores&a=nuevo_autor">Nuevo autor</a></li>
                                                <?php } ?>
                                        </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Libros<b class="caret"></b></a>						
                                    <ul class="dropdown-menu">
						<li class="dropdown-header">Lista</li>
						<li><a href="index.php?c=libros_controller&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="index.php?c=libros_controller&a=ver_lista&v=panel">Paneles</a></li>
                                                <?php if($_SESSION['rol']=='admin_level_1'){?>
						<li class="dropdown-header">Opciones</li>
						<li><a href="index.php?c=libros_controller&a=nuevo_libro">Nuevo Libro</a></li>
                                                <?php } ?>
					</ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Ejemplares<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Lista</li>
						<li><a href="index.php?c=ejemplares_controller&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="index.php?c=ejemplares_controller&a=ver_lista&v=panel">Paneles</a></li>
                                                <?php if($_SESSION['rol']=='admin_level_1'){?>
						<li class="dropdown-header">Opciones</li>
						<li><a href="index.php?c=ejemplares_controller&a=nuevo_ejemplar">Nuevo Ejemplar</a></li>
                                                <?php } ?>
					</ul>
                                </li>
                               
                                <li ><a href="index.php?c=inicio&a=cerrar_sesion" class="">Cerrar sesión</a>
                                </li>
			</ul>
                       
                  <!--termina la lista de elementos del menu*/-->
		</div>
            
             <?php } ?>
               
	</div>
</div>
