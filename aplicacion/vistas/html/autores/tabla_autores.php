﻿<?php if($_SESSION['rol']=='admin_level_1'){?>
<div class="row">
	<div class="col-md-2">
		<a href="index.php?c=autores&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="index.php?c=autores&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>
 <?php } ?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Autores</caption>
			<thead>
				<tr>
					<th>Autor</th>
					
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['autores'] as $autor) { ?>
    <tr>
					<td><?php echo $autor['nombre_autor']; ?></td>
                                        
					<td>
                                            <a	href="index.php?c=autores&a=ver_autor&v=tabla&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-primary btn-xs">Información</a> 
                                            <?php if($_SESSION['rol']=='admin_level_1'){?>
                                            <a	href="index.php?c=autores&a=editar_autor&v=tabla&id_autor=<?php echo $autor['id_autor']; ?>" class="btn btn-primary btn-xs">Editar</a> 
                                            <a	href="index.php?c=autores&a=borrar_autor&v=tabla&id_autor=<?php echo $autor['id_autor']; ?>"	class="btn btn-primary btn-xs">Borrar</a>
                                            <?php } ?>
                                        </td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>