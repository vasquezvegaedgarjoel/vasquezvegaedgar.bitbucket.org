<?php


require_once "aplicacion/librerias/bd/base_datos.php";

function select_usuario($user)
{
    
    $bd = obtener_conexion_base_datos();
    
    if ($bd['error'] == true) {
        return $bd;
    }
    
    $query = " select * from  usuarios  where  usuario = $1";
    
   
    $consulta = pg_query_params($bd['conexion'], $query, array(
        $user
    ));
    
    
    cerrar_conexion_base_datos($bd['conexion']);
    
    if ($consulta == false) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se ha podido obtener información del usuario'
            )
        );
    }
    
    
    if (pg_num_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No existe el usuario ' . $user
            )
        );
    }
    
    
    return array(
        'error' => false,
        'datos' => pg_fetch_all($consulta)
    );
}

