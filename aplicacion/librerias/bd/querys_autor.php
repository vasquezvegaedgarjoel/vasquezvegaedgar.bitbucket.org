<?php

/* Requerimos de acceso a la base de datos */
require_once "aplicacion/librerias/bd/base_datos.php";

function select_autor($id_autor)
{
    /* Obtenemos una conexión a la base de datos */
    $bd = obtener_conexion_base_datos();
    /*
     * Si durante la conexión se presentó algún error, lo "notificamos" al modelo que nos haya llamado.
     */
    if ($bd['error'] == true) {
        return $bd;
    }
    
    $query = " select * from  autores  where  id_autor = $1";
    
    /*
     * Ejecutamos la consulta, sobre la conexión abierta a la base de datos
     */
    $consulta = pg_query_params($bd['conexion'], $query, array(
        $id_autor
    ));
    
    /*
     * Antes de regresar los datos o el *posible error de consulta*, cerramos la conexión a la base de datos.
     */
    cerrar_conexion_base_datos($bd['conexion']);
    
    if ($consulta == false) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se ha podido obtener información del autor.'
            )
        );
    }
    
    /*
     * Si el número de filas (rows) contenidos en el resultado de la consulta es distinto a 1 se debe a que no fue encontrado el autor con el id indicado.
     */
    if (pg_num_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No existe el autor con id ' . $id_autor
            )
        );
    }
    
    /* Finalmente, regresamos los datos */
    return array(
        'error' => false,
        'datos' => pg_fetch_all($consulta)
    );
}

function insert_autor($autor)
{
    $bd = obtener_conexion_base_datos();
    if ($bd['error'] == true) {
        return $bd;
    }
    
    /* Atención al 'returning' */
    $query = "
        insert into
            autores
            (nombre_autor, nacionalidad_autor)
        values ($1, $2)
        returning id_autor
        ";
    
    $datos_nuevo_autor = array(
        $autor['nombre_autor'],
        $autor['nacionalidad_autor'],
    );
    
    $consulta = pg_query_params($bd['conexion'], $query, $datos_nuevo_autor);

    cerrar_conexion_base_datos($bd['conexion']);
    
    if ($consulta == false || pg_affected_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se han podido guardar los datos del autor.'
            ),
            'autor' => $autor
        );
    }
    
    /* Obtenemos el id asignado al nuevo autor por PostgreSQL */
    $nuevo_autor = pg_fetch_assoc($consulta);
    $autor['id_autor'] = $nuevo_autor['id_autor'];
    
    return array(
        'error' => false,
        'datos' => $autor
    );
}

function delete_autor($id_autor){
    
    $bd = obtener_conexion_base_datos();
    if ($bd['error'] == true) {
        return $bd;
    }
    
     /* Atención al 'returning' */
    $query = " delete from autores where id_autor=$1";
    
    $consulta = pg_query_params($bd['conexion'], $query, array($id_autor));
    
     cerrar_conexion_base_datos($bd['conexion']);
     
      if ($consulta == false || pg_affected_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se han podido eliminar los datos del autor.'
            ),
            'autor' => $id_autor
        );
    }
    
     
    return array(
        'error' => false,
        'datos' => $id_autor
    );
    
}


function update_autor($autor)
{
    $bd = obtener_conexion_base_datos();
    if ($bd['error'] == true) {
        return $bd;
    }
    $query = "update autores set nombre_autor=$1,nacionalidad_autor=$2 where id_autor=$3  ";    
    $datos_nuevo_autor = array(
        $autor['nombre_autor'],
        $autor['nacionalidad_autor'],
        $autor['id_autor'],
    );    
    $consulta = pg_query_params($bd['conexion'], $query, $datos_nuevo_autor);
    cerrar_conexion_base_datos($bd['conexion']);    
    if ($consulta == false || pg_affected_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se han podido actualizar los datos del autor.'
            ),
            'autor' => $autor
        );
    }
    $nuevo_autor = pg_fetch_assoc($consulta);
    $autor['id_autor'] = $nuevo_autor['id_autor'];
    
    return array(
        'error' => false,
        'datos' => $autor
    );
}
