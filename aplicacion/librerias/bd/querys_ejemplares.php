<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "aplicacion/librerias/bd/base_datos.php";

function select_ejemplares()
{
     /* Obtenemos una conexión a la base de datos */
    $bd = obtener_conexion_base_datos();
    /* Si durante la conexión se presentó algún error,
     * lo "notificamos" al modelo que nos haya llamado.
     */
    if ($bd['error'] == true) {
        return $bd;
    }
    
     $query = " select  * from  ejemplares order by id_ejemplar";
     
     $consulta = pg_query_params($bd['conexion'], $query, array());
     cerrar_conexion_base_datos($bd['conexion']);
     
     if ($consulta == false) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se ha podido obtener información de los autores.'
            )
        );
    }
    
    return array(
        'error' => false,
        'datos' => pg_fetch_all($consulta)
    );
}