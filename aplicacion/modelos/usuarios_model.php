<?php
function obtener_usuario($aplicacion, $user)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    
    if ( strlen($user) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El usuario no se encuentra';
    }
    
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    
    require_once "aplicacion/librerias/bd/querys_usuario.php";
    return select_usuario($user);
}
