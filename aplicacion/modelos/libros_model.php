<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function obtener_libros($aplicacion)
{
    require_once "aplicacion/librerias/bd/querys_libros.php";
    
    return select_libros();
}

function obtener_libro($aplicacion, $id_libro)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
   
    if (! is_numeric($id_libro) || strlen($id_libro) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El libro no existe.';
    }
    
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    
    require_once "aplicacion/librerias/bd/querys_libro.php";
    return select_libro($id_libro);
}

function guardar_datos_libro($aplicacion, $libro)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (empty($libro['titulo_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'no se a especificado el nombre del libro';
    }
    
   
    if (empty($libro['isbn_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el isbn.';
    }
    
     if (empty($libro['anio_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el año del libro.';
    }
    
    if (empty($libro['editorial_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado la editorial.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_libro.php";
    return insert_libro($libro);
}


function eliminar_libro($aplicacion,$id_libro){
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
     if (! is_numeric($id_libro) || strlen($id_libro) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El libro no existe.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_libro.php";
    
    return delete_libro($id_libro);
    
}


function actualizar_datos_libro($aplicacion, $libro){
    
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (empty($libro['isbn_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el isbn .';
    }
    
    /* ¡¡¡Observa el schema de la base de datos!!! */
    if (empty($libro['titulo_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el titulo.';
    }
    
       if (empty($libro['editorial_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado la editorial.';
    }
       if (empty($libro['anio_publicacion_libro'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el año de la publicacion.';
    }
   
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_libro.php";
    return update_libro($libro);
}





